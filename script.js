const urlDeckApi = "https://deckofcardsapi.com/api/deck/";
const botaoIniciar = document.querySelector("#iniciar");
const botaoNovaCarta = document.querySelector("#carta");
const botaoParar = document.querySelector("#parar");
const mesa = document.querySelector("#mesa");
const pontos = document.querySelector("#pontos");
const mensagem = document.querySelector("#idMensagem");
const pontuacao = document.querySelector("#idPontuacao");
let idBaralho;

function iniciarJogo(){
    obterBaralho();
    limparMesa();
}

function limparMesa(){
    pontos.innerHTML = 0;
    mesa.innerHTML = "";
    mensagem.innerHTML = "";
    mensagem.style.display = "none";
    pontuacao.style.display = "block";
    botaoIniciar.onclick = null;
}

function iniciarMesa(dados){
    idBaralho = dados.deck_id;
    abrirCartas(2);
}

function obterBaralho(){
    fetch(`${urlDeckApi}new/shuffle/`)
        .then(extrairJSON)
        .then(iniciarMesa);
}

function abrirCartas(numeroCartas){
    fetch(`${urlDeckApi}${idBaralho}/draw/?count=${numeroCartas}`)
        .then(extrairJSON)
        .then(preencherMesa);
}

function desenharCarta(carta){
    let desenho = document.createElement("img");
    let valor;

    desenho.src = carta.image;
    desenho.alt = carta.code;

    mesa.appendChild(desenho);

    switch(carta.value){
        case "ACE":
            valor = 1;
            break;
        case "JACK":
        case "QUEEN":
        case "KING":
            valor = 10;
            break;
        default:
            valor = Number(carta.value);
            break;
    }
    atualizarPlacar(valor);    
}

function atualizarPlacar(valor){
    const placarAnterior = Number(pontos.innerHTML) ;
    const placarAtual = placarAnterior + Number(valor);

    if(placarAtual == 21){
        exibirAlerta("Você venceu!");
    }
    else if(placarAtual >21){
        exibirAlerta(`Você perdeu com ${placarAtual}!`)
    }
    else{
        pontos.innerHTML = placarAtual;
    }
}

function pararJogo(){
    exibirAlerta(`Seu placar é de ${pontos.innerHTML}`);
}

function exibirAlerta(mensagemAlerta){
    mensagem.innerHTML = mensagemAlerta;
    mensagem.style.display = "block";
    pontuacao.style.display = "none";
    botaoIniciar.onclick = iniciarJogo;
}

function novaCarta(){
    abrirCartas(1);
}

function preencherMesa(dados){
    let cartas = dados.cards;
    for(let carta of cartas){
        desenharCarta(carta);
    }
}

function extrairJSON(resposta){
    return resposta.json();
}

botaoIniciar.onclick = iniciarJogo;
botaoNovaCarta.onclick = novaCarta;
botaoParar.onclick = pararJogo;